'use strict';

// Connect using WebSocket to the chat server address
var ws = new WebSocket("ws://127.0.0.1:1337/");

// Called when the connection is opened
ws.onopen = function() {
    console.log("Conexio oberta");

    // Send "Hello Server" string to the Server

};

// Called when a new message is received
ws.onmessage = function (evt) {
    console.log("client" + evt.data);
    var objecte = JSON.parse(evt.data);

        
    if(objecte.tipus == 2){
        document.getElementById("usuaris").innerHTML = "";
        
        for(let i = 0; i < objecte.usuaris.length; i++){
            document.getElementById("usuaris").innerHTML += objecte.usuaris[i] + "<br>";
        }
        
    } else if(objecte.tipus == 1){
        document.getElementById("missatges").innerHTML += objecte.usuari + ": " + objecte.missatge + "<br>";
        console.log("hola");
    }

    if(objecte.tipus == 6){
        document.getElementsByClassName("base")[0].style.display = "none";
        document.getElementsByClassName("base")[0].style.height = 0;
        document.getElementsByClassName("base")[0].style.width = 0;
        document.getElementById("login").style.display = "none";
        document.getElementById("register").style.display = "none";
        document.getElementById("canvas").style.display = "block";
        alert("Usuari connectat:  " + objecte.usuari);
    }

    if(objecte.tipus == 7){
        alert("Usuari o contrasenya incorrectes!");

    }

    if(objecte.tipus == 9){
        alert("Error en el cambi de nom!");
    }

    if(objecte.tipus == 11){
        document.getElementById("login").style.display = "block";
        document.getElementById("register").style.display = "none";
    }
    
};

// Called when the connection is closed
ws.onclose = function() {
    console.log("Conexio tancada");
};

// Called when an error with the connection happened
ws.onerror = function(err) {
    console.log("Error en la conexio");
};

ws.enviarMissatgeServer = function(){
    var missatge = document.getElementById("text").value;
    let missatgesServer = {
        tipus: 3,
        missatgeServer: ""
      }
    missatgesServer.missatgeServer = missatge;
    var enviarMissatge = JSON.stringify(missatgesServer);
    

    ws.send(enviarMissatge);
    document.getElementById("text").value = "";
};

ws.cambiarNom = function(){
    var nomNou = document.getElementById("nom").value;
    let nomServer = {
        tipus: 4,
        usuari: ""
    }
    nomServer.usuari = nomNou;
    var enviarNom = JSON.stringify(nomServer);
    ws.send(enviarNom);
    document.getElementById("nom").value = "";
};

ws.login = function(){
    var IdentificadorUsuari = document.getElementById("nick").value;
    var pass = document.getElementById("pass").value;
    let connexio = {
        tipus: 5,
        user: "",
        password: ""
    }
    connexio.user = IdentificadorUsuari;
    connexio.password = pass;
    var enviarConnexio = JSON.stringify(connexio);
    ws.send(enviarConnexio);
    document.getElementsByName("nick").value = "";
    document.getElementsByName("pass").value = "";
}

ws.vistaRegister = function(){
    document.getElementById("login").style.display = "none";
    document.getElementById("register").style.display = "block";
    
}

ws.registrar = function(){
    var correuIdentificador = document.getElementById("r-email").value;
    var nick = document.getElementById("r-nick").value;
    var pass = document.getElementById("r-pass").value;
    let registre = {
        tipus: 10,
        mail: "",
        user: "",
        password: ""
    }
    registre.mail = correuIdentificador;
    registre.user = nick;
    registre.password = pass;
    var enviarRegistre = JSON.stringify(registre);
    ws.send(enviarRegistre);


}

ws.vistaLogin = function(){
    document.getElementById("register").style.display = "none";
    document.getElementById("login").style.display = "block";
}

ws.enviarImatge = function(){
    var imatge = document.getElementById("imatge").value;
    console.log(typeof(imatge));
}
