'use strict';
var mysql = require('mysql');

class Connectar{

    constructor() {
      // We declare new attributes in here, ex:
      // this.foo = 0
     this.con = mysql.createConnection({
      host: "localhost",
      database: "chat",
      user: "root",
      password: ""
    });

    this.con.connect(function(err) {
        if (err) throw err;
        console.log("Connectat!");
    });
    
    }
    
    
    connectar(usuari, password, con, chatServer){
        var consulta = "SELECT * FROM usuaris WHERE correuIdentificador = '" + usuari + "';";
        this.con.query(consulta, function (err, result){
            if (err) throw err;
            if(result.length == 0){
                chatServer.loginIncorrecte(con, result[0]);
            }else{
                chatServer.login(con, result[0]);
            }
            
        });
    }

    registrar(mail ,usuari, password, con, chatServer){
        var consulta = "INSERT INTO usuaris VALUES('" + mail + "','" + usuari + "','" + password + "');";
        console.log(consulta);
        this.con.query(consulta, function (err, result){
            if (err) throw err;
            if(result.length == 0){
                chatServer.registreIncorrecte(con);
            }else{
                chatServer.registre(con);
            }
            
        });
    }

    canviarNom(usuari, con, chatServer, llistaClient){
        var correu = llistaClient.get(con).correu;
        var consulta = "UPDATE usuaris SET nick = '" + usuari + "'WHERE correuIdentificador = '" + correu + "';";
        console.log(consulta);
        console.log(usuari);
        this.con.query(consulta, function (err, result){
            if (err) throw err;
            if(result.length == 0){
                chatServer.canviIncorrecte(con, usuari);
            }else{
                chatServer.canviCorrecte(con, usuari);
            }
    });
    }

}

module.exports = Connectar;