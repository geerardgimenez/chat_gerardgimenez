'use strict';

var InfoClient = require('./InfoClient');
var BaseDades = require('./Connectar');

// This is our main server class
class ChatServer {

  constructor() {
    // We declare new attributes in here, ex:
    // this.foo = 0
    this.llistaClients = new Map();
    this.baseDades = new BaseDades();
    this.nomCon;
    this.numCon = 0;
    this.numCli = 0;
    this.nomCli;
  }

  // Called when a new connection 'con' is opened
  onConnectionOpen(con) {
    this.nomCon = "con" + this.numCon;
    this.numCon++;
    this.nomCli = "Client" + this.numCli;
    this.numCli++;
    var client = new InfoClient(this.nomCon, this.nomCli, false, "");
    console.log("Conexio establerta: ", con.remoteAddress);
    this.llistaClients.set(con, client);
    console.log("servidor" +this.llistaClients);
    this.notificarLlistaUsuaris(client);
    }
  

  // Called when connection 'con' is closed
  onConnectionClose(con) {
    console.log("Conexio tancada: ", con.remoteAddress);
    this.llistaClients.delete(con);
    this.notificarLlistaUsuaris(con);

  }

  // Called when a string message 'msg' from 'con' is received
  onConnectionMessage(con, msg) {
    var objecte = JSON.parse(msg);
    if(objecte.tipus == 3){
      let nomUsuari =  this.llistaClients.get(con).nomClient;
      this.enviarMissatge(con, objecte.missatgeServer, nomUsuari);
    }
    
    if(objecte.tipus == 4){
      this.baseDades.canviarNom(objecte.usuari, con, this, this.llistaClients);
    }

    if(objecte.tipus == 5){
      this.baseDades.connectar(objecte.user, objecte.password, con, this);
    }

    if(objecte.tipus == 10){
      this.baseDades.registrar(objecte.mail, objecte.user, objecte.password, con, this);
    }
    
    
  }

  enviarMissatge(con, msg, nomUsuari){
    
      let missatges = {
        tipus: 1,
        missatge: "",
        usuari: ""
      }
      missatges.missatge = msg;
      missatges.usuari = nomUsuari;
      var mis = JSON.stringify(missatges);
      this.llistaClients.forEach((valor, clau) => {
        if (valor.connectat){
        clau.sendUTF(mis);
        }
      });
    
  }

  notificarLlistaUsuaris()
  {
      let llistaUsuaris = {
        tipus: 2,
        usuaris: []
      }
      this.llistaClients.forEach((valor, clau) => {
      if(valor.connectat){
          llistaUsuaris.usuaris.push(valor.nomClient);
      }
      });
      console.log(llistaUsuaris);
      var llistaUsuarisString = JSON.stringify(llistaUsuaris);
      this.llistaClients.forEach((valor,clau) => 
      {
        console.log(valor.connectat);
        if(valor.connectat){
        clau.sendUTF(llistaUsuarisString);
        }
      });
  }


  login(con, result){
    
      this.llistaClients.get(con).connectat = true;
      let cambiarNom = {
        tipus: 6,
        usuari: result.nick
      }
      
      var missatge = JSON.stringify(cambiarNom);
      this.llistaClients.get(con).nomClient = result.nick
      this.llistaClients.get(con).correu = result.correuIdentificador
      console.log(this.llistaClients.get(con));
      this.notificarLlistaUsuaris(con);
      con.sendUTF(missatge);
    

  }

  loginIncorrecte(con, result){
    let loginIncorrecte = {
      tipus: 7,
    }
    
    var missatge = JSON.stringify(loginIncorrecte);
    con.sendUTF(missatge);
  }

  canviCorrecte(con, usuariNou){
    let actualitzarNom = {
      tipus: 8,
      usuari: usuariNou
    }
    this.llistaClients.get(con).nomClient = usuariNou
    this.notificarLlistaUsuaris(con);
    var missatge = JSON.stringify(actualitzarNom);
    con.sendUTF(missatge);

    
  }

  canviIncorrecte(con, usuari){
    let canviIncorrecte = {
      tipus: 9,
    }
    var missatge = JSON.stringify(canviIncorrecte);
    con.sendUTF(missatge);
  }

  registre(con){
    let registre = {
      tipus: 11
    }
    var missatge = JSON.stringify(registre);
    con.sendUTF(missatge);
  }


  
  
}



  


// We export our class so we can import it later with 'require'
module.exports = ChatServer;
